// Baitap3
function incomeTaxed(yearIncome, dependentPerson) {
  return yearIncome - 4e6 - dependentPerson * 16e5;
}
function returnTax(a) {
  if (a <= 60e6) {
    return 0.05;
  }
  if (a <= 120e6) {
    return 0.1;
  }
  if (a <= 210e6) {
    return 0.15;
  }
  if (a <= 384e6) {
    return 0.2;
  }
  if (a <= 624e6) {
    return 0.25;
  }
  if (a <= 960e6) {
    return 0.25;
  }
  if (a <= 960e6) {
    return 0.25;
  }
}
function taxBill() {
  var userName2 = document.getElementById("userName2").value;
  if (userName2 == "") {
    return alert("Vui lòng nhập tên của bạn");
  }
  var yearIncome = document.getElementById("yearIncome").value * 1;
  if (yearIncome < 0) {
    return alert("Số tiền thu nhập cần là số dương. Vui lòng nhập lại!");
  }
  var dependentPerson = document.getElementById("dependentPerson").value * 1;
  if (dependentPerson < 0 || Number.isInteger(dependentPerson) == false) {
    return alert(
      "Số người phụ thuộc cần là số NGUYÊN DƯƠNG. Vui lòng nhập lại!"
    );
  }
  var result;
  var a = incomeTaxed(yearIncome, dependentPerson);
  if (a <= 0) {
    document.getElementById("taxBill").innerHTML =
      "Poor you! Bạn còn không đủ điều kiện để đóng thuế thu nhập :< Bạn quá nghèo";
  } else {
    if (a <= 60e6) {
      result = a * returnTax(a);
    } else if (a <= 120e6) {
      result = 60e6 * 0.05 + (a - 60e6) * returnTax(a);
    } else if (a <= 210e6) {
      result = 60e6 * 0.05 + 60e6 * 0.1 + (a - 120e6) * returnTax(a);
    } else if (a <= 384e6) {
      result =
        60e6 * 0.05 + 60e6 * 0.1 + 90e6 * 0.15 + (a - 210e6) * returnTax(a);
    } else if (a <= 624e6) {
      result =
        60e6 * 0.05 +
        60e6 * 0.1 +
        90e6 * 0.15 +
        174e6 * 0.2 +
        (a - 384e6) * returnTax(a);
    } else if (a <= 960e6) {
      result =
        60e6 * 0.05 +
        60e6 * 0.1 +
        90e6 * 0.15 +
        174e6 * 0.2 +
        240e6 * 0.25 +
        (a - 624e6) * returnTax(a);
    } else {
      result =
        60e6 * 0.05 +
        60e6 * 0.1 +
        90e6 * 0.15 +
        174e6 * 0.2 +
        240e6 * 0.25 +
        336e6 * 0.3 +
        (a - 960e6) * returnTax(a);
    }
    var subResult = Intl.NumberFormat("de-DE", {
      style: "currency",
      currency: "VND",
    }).format(result);
    document.getElementById(
      "taxBill"
    ).innerHTML = `<b style="color: #FFF000">${userName2.toUpperCase()}</b> cần đóng thuế thu nhập cá nhân là <b style="color: #FFF000">${subResult}</b>.`;
  }
}
// Baitap4
const typeOfCustumer = document.getElementById("customerType");
typeOfCustumer.addEventListener("click", function () {
  if (typeOfCustumer.value == "DN") {
    document.getElementById("numberOfConnections").style.display = "block";
  } else {
    document.getElementById("numberOfConnections").style.display = "none";
  }
});
function totalFeeCN(numberofChannel) {
  return 4.5 + 20.5 + 7.5 * numberofChannel;
}
function totalFeeDN(numberofChannel, numberOfConnections) {
  if (numberOfConnections <= 10) {
    return 15 + 75 + 50 * numberofChannel;
  } else {
    return 15 + 50 * numberofChannel + 75 + (numberOfConnections - 10) * 5;
  }
}
function cableBill() {
  var customerType = document.getElementById("customerType").value;
  if (customerType == "Chọn loại khách hàng") {
    return alert(`Vui lòng chọn loại khách hàng`);
  }
  var userCode = document.getElementById("userCode").value;
  if (userCode == "") {
    return alert(`Vui lòng nhập mã khách hàng`);
  }
  var numberofChannel = document.getElementById("numberofChannel").value * 1;
  if (numberofChannel <= 0 || Number.isInteger(numberofChannel) == false) {
    return alert(`Số kênh cao cấp không hợp lệ.
        Vui lòng nhập lại, số kênh cao cấp cần là số nguyên dương.`);
  }
  var result;
  if (customerType == "CN") {
    result = totalFeeCN(numberofChannel);
  } else {
    var numberOfConnections =
      document.getElementById("numberOfConnections").value * 1;
    if (
      numberOfConnections < 0 ||
      Number.isInteger(numberOfConnections) == false
    ) {
      return alert(` Số kết nối không hợp lệ
            Vui lòng nhập số nguyên dương.`);
    } else {
      result = totalFeeDN(numberofChannel, numberOfConnections);
    }
  }
  var subResult = Intl.NumberFormat("ja-US", {
    style: "currency",
    currency: "USD",
  }).format(result);
  document.getElementById(
    "cableBill"
  ).innerHTML = ` Mã khách hàng: <b style="color: #FFF000">${userCode}</b>
                                                        <br/>Tiền cáp: <b style="color: #FFF000">${subResult}</b>`;
}
